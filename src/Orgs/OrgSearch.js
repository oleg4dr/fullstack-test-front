import React, {Component} from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Container from 'react-bootstrap/Container'
import Table from 'react-bootstrap/Table'

const TableHeader = () => {
    return (
      <thead>
        <tr>
          <th>Организация</th>
          <th>Родитель</th>       
        </tr>
      </thead>
    )
  }

   

export default class OrgSearchForm extends Component{
    state ={
        data: [],
        searchKey:"",
    }

    handleChange = event => {
        const {name, value} = event.target

        this.setState({
            [name]:value,
        })
    }

    searchOrg = value => {
        fetch('/api/searchOrg/' + value)
        .then(result => result.json())
        .then(result => {
            let options = [];
            result.map(el => {
                if(el.orgParentId===null)
                  el.orgParentId="";
                options.push(
                    <tr key = {el.orgId}>
                        <td>{el.orgTitle}</td>
                        <td>{el.parentName}</td>
                    </tr>)
            })
            this.setState({
                data: options,
            })
        })
    }

    render(){
        return(
            <Container>
                <Form>
                <Form.Group>
                    <Form.Label>Организация</Form.Label>
                    <Form.Control
                     type="text"
                     placeholder="Введите имя организации"
                     name="searchKey"
                     value={this.state.searchKey}
                     onChange={this.handleChange} 
                    />
                </Form.Group>   
                <Button variant="outline-primary" onClick={() => this.searchOrg(this.state.searchKey)}>Поиск</Button>
            </Form>
                <Table bordered hover>
                    <TableHeader/>
                    <tbody>{this.state.data}</tbody>
                </Table>
            </Container>
        )
    }


}