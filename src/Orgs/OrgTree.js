import React, {Component} from 'react'
import Container from 'react-bootstrap/Container'
import ListGroup from 'react-bootstrap/ListGroup'

export default class OrgTree extends Component {
  state={
    treeData:[],
  }

  componentDidMount(){
    fetch('/api/getFullOrgTree')
    .then(result => result.json())
    .then(result => {
    this.setState({ 
        treeData: result
        })
    })
  }

  createTree(data, isLeaf, level = 1){
    let childNodes = [];
    if (isLeaf) level++;
    data.map(node => {
      if (node.children.length > 0){ //есть дети идем глубже!
        childNodes.push(
          <ListGroup.Item key={node.orgId}>
              +{node.orgTitle}
              <ListGroup variant="flush" >     
                {this.createTree(node.children, true, level)}
              </ListGroup>
          </ListGroup.Item>
        );
      } else { //детей не найдено - листочек
        childNodes.push(
          <ListGroup.Item key={node.orgId}>
            {node.orgTitle}
          </ListGroup.Item>
        );
      }
    })
    return childNodes;
  }
    render(){
        return(      
          <Container>
            <ListGroup variant="flush">
              {this.createTree(this.state.treeData, false)}
            </ListGroup>
          </Container>
        );
    }
}
