import React, {Component} from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

export default class OrgEditForm extends Component{
    initialState = {
        id: this.props.selectedCompany.orgId,
        title: this.props.selectedCompany.orgTitle,
        parentId: this.props.selectedCompany.orgParentId,
        options:[],
    }

    state = this.initialState
    
    componentDidMount(){
        fetch('/api/getValidParentOrgs/' + this.props.selectedCompany.orgId)
        .then(result => result.json())
        .then(result => {
            let options=[];
          result.map(el=> {
            options.push(<option key={el.orgId} value={el.orgId}>{el.orgTitle}</option>)
          })
          this.setState({ 
            options: options
            })
        })
    }

    handleChange = event => {
        const {name, value} = event.target

        this.setState({
            [name]:value,
        })
    }
    
    submitForm = () => {
        this.props.handleEdit(this.state)
        this.props.closeForm()
    }

    render(){
        const {title, parentId} = this.state
        
        return(
            <Form>
                <Form.Group>
                    <Form.Label>Название</Form.Label>
                    <Form.Control
                     type="text"
                     placeholder="Введите название"
                     name="title"
                     id="name"
                     value={title}
                     onChange={this.handleChange} 
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Родительская организация</Form.Label>
                    <Form.Control as="select" custom name="parentId" id="parent" 
                    value={parentId} onChange={this.handleChange}>
                        <option  key="NoParent" value="">Отсутствует</option>   
                        {this.state.options}
                    </Form.Control>
                </Form.Group>     
                <Button variant="outline-primary" onClick={this.submitForm}>Изменить</Button>
            </Form>
        );
    }
}