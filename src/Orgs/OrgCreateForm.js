import React, {Component} from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

class OrgCreateForm extends Component{
    initialState = {
        name:'',
        value:'',
        options:[],
    }

    state = this.initialState
    
    handleChange = event => {
        const {name, value} = event.target

        this.setState({
            [name]:value,
        })
    }

    componentDidMount(){
        fetch('/api/getOrgs')
        .then(result => result.json())
        .then(result => {
            let options=[];
          result.map(el=> {
            options.push(<option key={el.orgId} value={el.orgId}>{el.orgTitle}</option>)
          })
          this.setState({ 
            options: options
            })
        })
    }
    
    submitForm = () => {
        this.props.handleSubmit(this.state)     
        this.props.closeForm()
    }

    render(){
        const {name} = this.state
        
        return(
            <Form>
                <Form.Group>
                    <Form.Label>Название</Form.Label>
                    <Form.Control
                     type="text"
                     placeholder="Введите название"
                     name="name"
                     id="name"
                     value={name}
                     onChange={this.handleChange} 
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Родительская организация</Form.Label>
                    <Form.Control as="select" custom name="value" id="parent" value={this.state.value} onChange={this.handleChange}>
                        <option  key="NoParent" value=''>Отсутствует</option>   
                        {this.state.options}
                    </Form.Control>
                </Form.Group>     
                <Button variant="outline-primary" onClick={this.submitForm}>Добавить</Button>
            </Form>
        );
    }
}

export default OrgCreateForm;