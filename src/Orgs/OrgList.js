import React, { Component } from 'react'
import OrgCreateForm from './OrgCreateForm'
import OrgEditForm from './OrgEditForm'
import Table from 'react-bootstrap/Table'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import OrgSearchForm from './OrgSearch'
import Pagination from 'react-bootstrap/Pagination'
import Container from 'react-bootstrap/Container'

const TableHeader = () => {
  return (
    <thead>
      <tr>
        <th>Организация</th>
        <th>Родитель</th>
        <th></th>
        <th></th>
      </tr>
    </thead>
  )
}

const TableBody = props => {

  const rows = props.orgData.map(row => {
    return (
      <tr key={row.orgId}>
        <td>{row.orgTitle}</td>
        <td>{row.parentName}</td>
        <td className="control-btn">
          <Button variant="outline-info" onClick={() => props.editOrg(row)}>
            Изменить
          </Button>
        </td>
        <td className="control-btn">
          <Button
           variant="outline-danger"
           onClick={() => props.removeOrg(row)}
          >
              Удалить
          </Button>
        </td>
      </tr>
    )
  })

  return <tbody>{rows}</tbody>
}

const PagintationBasic = props => {
  let items=[];
  for (let number = 1; number <= props.total; number++) {
    items.push(
      <Pagination.Item key={number} active={number === props.active} onClick={()=>props.changePage(number)}>
        {number}
      </Pagination.Item>,
    );
  }

  return <Pagination>{items}</Pagination>
}

class OrgList extends Component {
  state = {
    data: [],
    EditShow: false,
    show:false,
    selectedCompany:"",
    showSearch: false,
    pageCount:"",
    activePage:"",
    pagination:[],
  }

  componentDidMount() {
    fetch('/api/getOrgs/1')
      .then(result => result.json())
      .then(result => {
        result.map(el=> {
          if (el.orgParentId===null)
            el.orgParentId="";     
        })
        this.setState({ 
            data: result,
            activePage:1,
          })
      })

    fetch('api/getOrgPageCount')
    .then(result => result.json())
    .then(result => {
      this.setState({
      pageCount: result
        })
      let items=[];
        for (let number = 1; number <= this.state.pageCount; number++) {
          items.push(
        <Pagination.Item key={number} active={number === this.state.activePage} onClick={()=>this.changePage(number)}>
          {number}
        </Pagination.Item>,
        );
      }
      this.setState({
        pagination:items,
      }) 
    })
  }

  handleSubmit = org => {
    fetch('/api/addOrg', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "orgTitle": org.name,
        "orgParentId": org.value,
      })
    })
    .then((response) => {
      return response.json();
    }).then((result) => {
      if(this.state.data.length<5){
        if (result.orgParentId===null){
          result.orgParentId="";
          result.parentName="";
        }   
        this.setState({data: [...this.state.data, result]})
      }
      if(this.state.data.length==5 && this.state.activePage==this.state.pageCount){
        let newP = 
        <Pagination.Item key={this.state.pageCount++} active={this.state.pageCount++ === this.state.activePage} onClick={()=>this.changePage(this.state.pageCount++)}>
          {this.state.pageCount++}
        </Pagination.Item>
        this.setState({
          pagination: [...this.state.pagination, newP]
        })
      }
    });
  }

  handleEdit = org => {
    const { data } = this.state

    fetch("/api/editOrg/" , {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "orgId": org.id,
        "orgTitle": org.title,
        "orgParentId": org.parentId,
      })
    })
    .then((response) => {
      return response.json();
    }).then((result) => {
      if (result.orgParentId===null){
        result.orgParentId="";
        result.parentName="";
      }
      this.setState({
        data: data.map(p=>{  
          if(p.orgParentId===result.orgId) 
            return {...p, parentName: result.orgTitle}
          else
            return p.orgId===result.orgId
              ? {...p, orgTitle: result.orgTitle, orgParentId: result.orgParentId, parentName: result.parentName}: p})
      })
    });
  }

  removeOrg = target => {
    const { data } = this.state
    // if(target.orgParentId!="")
    //   return alert("Нельзя удалять организацию у которой есть дочерние компании!");
    // можно через data.find найти наличие детей для костыльного 
    fetch("/api/delOrg/" + target.orgId, {
      method: 'DELETE'
    });

    this.setState({
      data: data.filter(org => {
        return org.orgId !== target.orgId
      }),
    })
  }

  selectCompany = targetCompany => {
    const { data } = this.state

    this.setState({
      selectedCompany: targetCompany,
      EditShow: true,
    })
  }

  changePage = page => {
    fetch('api/getOrgs/' + page)
    .then(result => result.json())
    .then(result => {
      result.map(el=> {
        if (el.orgParentId===null)
          el.orgParentId="";     
      })   
      this.setState({
        data: result,
        activePage: page
      })
    })
      
  }

  render() {
    const { data } = this.state

    const handleClose = () => this.setState({
      show: false
    })
    const handleEditClose = () => this.setState({
      EditShow: false,
    })

    const handleSearchClose = () => this.setState({
      showSearch: false,
    })

    const handleShow = () => this.setState({
      show: true
    })

    const handleSearchShow = () => this.setState({
      showSearch: true
    })

    return (
      <div className="container">
        <Pagination>{this.state.pagination}</Pagination>
        <Table bordered hover>
          <TableHeader/>
          <TableBody orgData={data} removeOrg={this.removeOrg} editOrg={this.selectCompany} />
        </Table>
        <Button variant="primary" onClick={handleShow}>
          Создать организацию
        </Button>
        <Button variant="primary" onClick={handleSearchShow}>
          Найти организацию
        </Button>
      <Modal show={this.state.show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Создать организацию</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <OrgCreateForm closeForm={handleClose} handleSubmit={this.handleSubmit} />
        </Modal.Body>
      </Modal>
      <Modal show={this.state.EditShow} onHide={handleEditClose}>
        <Modal.Header closeButton>
          <Modal.Title>Изменить организацию</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <OrgEditForm closeForm={handleEditClose} selectedCompany={this.state.selectedCompany} 
          handleEdit={this.handleEdit} />
        </Modal.Body>
      </Modal>
      <Modal show={this.state.showSearch} onHide={handleSearchClose}>
        <Modal.Header closeButton>
          <Modal.Title>Найти организацию</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <OrgSearchForm/>
        </Modal.Body>
      </Modal>
      </div>
    )
  }
}

export default OrgList