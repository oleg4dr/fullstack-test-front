import React, {Component} from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

export default class WorkerCreateForm extends Component{
    initialState = {
        name:'',
        org:'',
        lead:'',
        leads:[],
        orgs:[],
    }

    state = this.initialState

    handleChange = event => {
        const {name, value} = event.target

        this.setState({
            [name]:value,
        })
    }

    submitForm = () => {
        this.props.handleSubmit(this.state)
        this.setState(this.initialState)
    }

    getViableLeads = event => {
        const {name, value} = event.target

        this.setState({
            [name]:value,
        })

        fetch('/api/getLeads/' + value)
        .then(result => result.json())
        .then(result => {
            let options=[];
          result.map(el=> {
            if (el.empLeadId===null){
              el.empLeadId="";
            }
            options.push(<option key={el.empId} value={el.empId}>{el.empName}</option>)
          })
          this.setState({ 
            leads: options
            })
        })
    }

    componentDidMount(){
        fetch('/api/getOrgs')
      .then(result => result.json())
      .then(result => {
          let options=[];
        result.map(el=> {
          if (el.orgParentId===null){
            el.orgParentId="";
            el.parentName="";
          }
          options.push(<option key={el.orgId} value={el.orgId}>{el.orgTitle}</option>)
        })
        this.setState({ 
            orgs: options
          })
      })
    }

    render(){
        const {name} = this.state
        
        return(
            <Form>
                <Form.Group>
                    <Form.Label>Сотрудник</Form.Label>
                    <Form.Control
                     type="text"
                     placeholder="Введите имя"
                     name="name"
                     id="name"
                     value={name}
                     onChange={this.handleChange} 
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Организация</Form.Label>
                    <Form.Control as="select" custom name="org" value={this.state.org} onChange={this.getViableLeads} >
                        <option  key="noOrg" value=''>Отсутствует</option>   
                        {this.state.orgs}
                    </Form.Control>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Руководитель</Form.Label>
                    <Form.Control as="select" custom name="lead" value={this.state.lead} onChange={this.handleChange} >
                        <option  key="noLead" value=''>Отсутствует</option>
                        {this.state.leads}   
                    </Form.Control>
                </Form.Group>         
                <Button variant="outline-primary" onClick={this.submitForm}>Добавить</Button>
            </Form>
        );
    }
}