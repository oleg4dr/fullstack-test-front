import React, {Component} from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Container from 'react-bootstrap/Container'
import Table from 'react-bootstrap/Table'

const TableHeader = () => {
    return (
      <thead>
        <tr>
          <th>Сотрудник</th>
          <th>Организация</th>
          <th>Начальник</th>           
        </tr>
      </thead>
    )
  }

   

export default class WorkerSearchForm extends Component{
    state ={
        data: [],
        searchKeyOrg:"",
        searchKeyName:"",
    }

    handleChange = event => {
        const {name, value} = event.target

        this.setState({
            [name]:value,
        })
    }

    searchOrg = isOr => {
        fetch('/api/searchWorkers', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              "org": this.state.searchKeyOrg,
              "name": this.state.searchKeyName,
              "isOr": isOr,
            })
          })
        .then(result => result.json())
        .then(result => {
            let options = [];
            result.map(el => {
                if(el.empOrgId===null)
                  el.empOrgId="";
                if(el.empLeadId===null)
                  el.empLeadId="";
                options.push(
                    <tr key = {el.empId}>
                        <td>{el.empName}</td>
                        <td>{el.orgName}</td>
                        <td>{el.leadName}</td>
                    </tr>)
            })
            this.setState({
                data: options,
            })
        })
    }

    render(){
        return(
            <Container>
                <Form>
                <Form.Group>
                    <Form.Label>Организация</Form.Label>
                    <Form.Control
                     type="text"
                     placeholder="Введите имя организации"
                     name="searchKeyOrg"
                     value={this.state.searchKeyOrg}
                     onChange={this.handleChange} 
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Сотрудник</Form.Label>
                    <Form.Control
                     type="text"
                     placeholder="Введите имя сотрудника"
                     name="searchKeyName"
                     value={this.state.searchKeyName}
                     onChange={this.handleChange} 
                    />
                </Form.Group>      
                <Button variant="outline-primary" onClick={()=>this.searchOrg(false)}>Поиск "И"</Button>
                <Button variant="outline-primary" onClick={()=>this.searchOrg(true)}>Поиск "ИЛИ"</Button>
            </Form>
                <Table bordered hover>
                    <TableHeader/>
                    <tbody>{this.state.data}</tbody>
                </Table>
            </Container>
        )
    }


}