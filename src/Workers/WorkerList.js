import React, { Component } from 'react'
import Table from 'react-bootstrap/Table'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import WorkerCreateForm from './WorkerCreateForm'
import WorkerEditForm from './WorkerEditForm'
import Pagination from 'react-bootstrap/Pagination'
import Container from 'react-bootstrap/Container'
import WorkerSearchForm from './WorkerSearh'

const TableHeader = () => {
    return (
      <thead>
        <tr>
          <th>Сотрудник</th>
          <th>Организация</th>
          <th>Начальник</th>
          <th></th>
          <th></th>
        </tr>
      </thead>
    )
  }

  const TableBody = props => {

    const rows = props.workerData.map(row => {
      return (
        <tr key={row.empId}>
          <td>{row.empName}</td>
          <td>{row.orgName}</td>
          <td>{row.leadName}</td>
          <td className="control-btn">
            <Button 
              variant="outline-info"
              onClick={()=>props.editWorker(row)}
            >
                Изменить
            </Button>
          </td>
          <td>
            <Button 
              variant="outline-danger"
              onClick={() => props.removeWorker(row.empId)}
            >
                Удалить
            </Button>
          </td>
        </tr>
      )
    })
  
    return <tbody>{rows}</tbody>
  }

  const PagintationBasic = props => {
    let items=[];
    for (let number = 1; number <= props.total; number++) {
      items.push(
        <Pagination.Item key={number} active={number === props.active} onClick={()=>props.changePage(number)}>
          {number}
        </Pagination.Item>,
      );
    }

    return <Pagination>{items}</Pagination>
  }




  export default class WorkerList extends Component {
    state = {
        data: [],
        showCreate: false,
        showEdit: false,
        showSearch: false,
        selectedWorker: "",
        pageCount:"",
        activePage:"",
      }

    componentDidMount() {
      fetch('api/getWorkers/1')
          .then(result => result.json())
          .then(result => {
            result.map(el=> {
              if (el.empLeadId===null){
                el.empLeadId="";
                el.leadName="";
              }
              if (el.empOrgId===null){
                el.orgName="";
                el.empOrgId="";
              }
            })
          this.setState({ 
              data: result,
              activePage:1,
              })
          })

      fetch('api/getWorkerPageCount')
          .then(result => result.json())
          .then(result =>
            this.setState({
              pageCount: result
            }) )
    }

    changePage = page => {
      fetch('api/getWorkers/' + page)
      .then(result => result.json())
      .then(result =>{
        result.map(el=> {
          if (el.empLeadId===null)
            el.empLeadId="";       
          if (el.empOrgId===null)  
            el.empOrgId="";
        })
         this.setState({
          data: result,
          activePage: page
        })
      })
    }

    selectWorker = targetWorker => {
      this.setState({
        showEdit:true,
        selectedWorker: targetWorker,
      })
    }

    removeWorker = targetWorkerId => {
      const { data } = this.state
  
      fetch("/api/delWorker/" + targetWorkerId, {
        method: 'DELETE'
      });
  
      this.setState({
        data: data.filter(worker => {
          return worker.empId !== targetWorkerId
        }),
      })
    }

    submitWorker = worker => {
      fetch("/api/addWorker", {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "empName": worker.name,
          "empOrgId": worker.org,
          "empLeadId": worker.lead,
        })
      })
      .then((response) => {
        return response.json();
      }).then((result) => {
        console.log(result)
        if (result.empOrgId===null){
          result.empOrgId="";
          result.orgName="";
        }
        if (result.empLeadId===null){
          result.empLeadId="";
          result.leadName="";
        }
          
        this.setState({data: [...this.state.data, result]})
      });
    }

    editWorker = worker => {
      const { data } = this.state
  
      fetch("/api/editWorker/" , {
        method: 'PUT',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "empId": worker.empId,
          "empName": worker.empName,
          "empOrgId": worker.orgId,
          "empLeadId": worker.leadId,
        })
      })
      .then((response) => {
        return response.json();
      }).then((result) => {
        if (result.empOrgId===null){
          result.empOrgId="";
        }
        if (result.empLeadId===null){
          result.empLeadId="";
        }
        this.setState({
          data: data.map(p=>{  
            if(p.empLeadId===result.empId) 
              return {...p, leadName: result.leadName}
            else
              return p.empId===result.empId
                ? {...p, empName: result.empName, empOrgId: result.empOrgId, 
                  orgName: result.orgName, empLeadId: result.empLeadId, leadName: result.leadName}: p})
        })
      });
    }

    

    render() {
        const { data } = this.state

        const showCreateForm = () => this.setState({
          showCreate: true
        })
        const hideCreateForm = () => this.setState({
          showCreate: false
        })
        const hideEditForm = () => this.setState({
          showEdit: false
        })
        const handleSearchShow = () => this.setState({
          showSearch: true
        })
        const handleSearchClose = () => this.setState({
          showSearch: false,
        })

        return (
          <div className="container">
            <Container ><PagintationBasic changePage={this.changePage} active={this.state.activePage} total={this.state.pageCount}/></Container>
            <Button variant="primary" onClick={showCreateForm}>
                Создать сотрудника
            </Button>
            <Button variant="primary" onClick={handleSearchShow}>
                Найти сотрудника
            </Button>
            <Table bordered hover>
                <TableHeader/>
                <TableBody removeWorker={this.removeWorker} workerData={data} editWorker={this.selectWorker} />
            </Table>
            <Modal show={this.state.showCreate} onHide={hideCreateForm}>
              <Modal.Header closeButton>
                <Modal.Title>Создать сотрудника</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <WorkerCreateForm handleSubmit={this.submitWorker}/>
              </Modal.Body>
            </Modal>
            <Modal show={this.state.showEdit} onHide={hideEditForm}>
              <Modal.Header closeButton>
                <Modal.Title>Изменить сотрудника</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <WorkerEditForm handleEdit={this.editWorker} closeForm={hideEditForm} 
                selectedWorker={this.state.selectedWorker}/>
              </Modal.Body>
            </Modal>
            <Modal show={this.state.showSearch} onHide={handleSearchClose}>
            <Modal.Header closeButton>
              <Modal.Title>Найти Сотрудника</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <WorkerSearchForm/>
            </Modal.Body>
            </Modal>
              
          </div>
        )
    }
  }