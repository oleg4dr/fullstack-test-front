import React, {Component} from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

export default class WorkerEditForm extends Component{
    initialState = {
        empId: this.props.selectedWorker.empId,
        empName: this.props.selectedWorker.empName,
        orgId: this.props.selectedWorker.empOrgId,
        orgName: this.props.selectedWorker.orgName,
        leadId: this.props.selectedWorker.empLeadId,
        leadName: this.props.selectedWorker.leadName,
        leads:[],
        orgs:[],
        initialOrg: <option  key= {this.orgId} value={this.orgId}>{this.orgName}</option> ,
        initialLead: <option  key={this.leadId} value={this.leadId}>{this.leadName}</option>,
    }

    state = this.initialState

    handleChange = event => {
        const {name, value} = event.target

        this.setState({
            [name]:value,
        })
    }

    getViableLeads = event => {
        const {name, value} = event.target

        this.setState({
            [name]:value,
        })

        fetch('/api/getLeads/' + value)
        .then(result => result.json())
        .then(result => {
          if(result.length === undefined)
           return;  //убить лида
          else {
            let options=[];
          result.map(el=> {
            if (el.empLeadId===null){
              el.empLeadId="";
            }           
            options.push(<option key={el.empId} value={el.empId}>{el.empName}</option>)
          })
          this.setState({ 
            leads: options
            })
          }     
        })
    }

    componentDidMount(){
        fetch('/api/getOrgs')
      .then(result => result.json())
      .then(result => {
          let options=[];
        result.map(el=> {
          if (el.orgParentId===null){
            el.orgParentId="";
            el.parentName="";
          }
            options.push(<option key={el.orgId} value={el.orgId}>{el.orgTitle}</option>)
        })
        this.setState({ 
            orgs: options
          })
      })

      fetch('/api/getLeads/' + this.state.orgId)
      .then(result => result.json())
      .then(result => {
        if(result.length === undefined)
           return;
          let options=[];
        result.map(el=> {
          if (el.empLeadId===null){
            el.empLeadId="";
          }
          if(el.empId!==this.state.leadId)           
          options.push(<option key={el.empId} value={el.empId}>{el.empName}</option>)
        })
        this.setState({ 
          leads: options
          })
      })
    }

    submitForm = () => {
      this.props.handleEdit(this.state)
      this.props.closeForm()
  }

    render(){
        const {empName} = this.state
        
        return(
            <Form>
                <Form.Group>
                    <Form.Label>Сотрудник</Form.Label>
                    <Form.Control
                     type="text"
                     placeholder="Введите имя"
                     name="empName"
                     id="name"
                     value={empName}
                     onChange={this.handleChange} 
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Организация</Form.Label>
                    <Form.Control as="select" custom name="orgId" value={this.state.orgId} onChange={this.getViableLeads} >
                        <option  key="noOrg" value=''>Отсутствует</option> 
                        {/* <option  key={this.state.orgId} value={this.state.orgId}>{this.state.orgName}</option>    */}
                        {this.state.orgs}
                    </Form.Control>
                </Form.Group>
                <Form.Group>
                    <Form.Label>Руководитель</Form.Label>
                    <Form.Control as="select" custom name="leadId" value={this.state.leadId} onChange={this.handleChange} >
                        <option  key="noLead" value=''>Отсутствует</option>
                        {this.state.leads}   
                    </Form.Control>
                </Form.Group>         
                <Button variant="outline-primary" onClick={this.submitForm}>Изменить</Button>
            </Form>
        );
    }
}