/* Import statements */
import React from "react";
import { Link, Route } from "react-router-dom";
import OrgList from "./Orgs/OrgList"
import WorkerList from "./Workers/WorkerList"
import Nav from 'react-bootstrap/Nav'
import OrgTree from './Orgs/OrgTree'
import WorkerTree from './Workers/WorkerTree'

export default function App() {
  return (
    <div>
      <Nav fill variant="tabs" defaultActiveKey="/orgs">
        <Nav.Item>
          <Nav.Link as={Link} to="/orgs">
            Организации
          </Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link as={Link} to="/workers" >
            Сотрудники
          </Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link as={Link} to="/orgTree" >
            Дерево организаций
          </Nav.Link>
        </Nav.Item>
        <Nav.Item>
          <Nav.Link as={Link} to="/workerTree" >
            Дерево сотрудников
          </Nav.Link>
        </Nav.Item>
      </Nav>  
      <Route path="/orgs" component={OrgList} />
      <Route path="/workers" component={WorkerList} />
      <Route path="/orgTree" component={OrgTree}/>
      <Route path="/workerTree" component={WorkerTree}/>
    </div>
  );
}
