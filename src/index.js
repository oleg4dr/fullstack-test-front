import React from 'react'
import ReactDOM from 'react-dom'
import App from './Router'
import {BrowserRouter} from "react-router-dom"
//дневная тема
// import 'bootstrap/dist/css/bootstrap.min.css';
//ночная тема
import './bootstrap.min.css';
import "./index.css"

ReactDOM.render(
    <BrowserRouter>
        <App />
    </BrowserRouter>, 
    document.getElementById('root')
)